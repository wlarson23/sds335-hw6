#include <iostream>
using namespace std;

//template allows machine epsilon to be calculated for different precisions
template <class e>
//function to calculate epsilon
e get_epsilon(){
	e epsilon, next_value=0.5;

	//e = 2^(-m) loop until 1 + e = 1
	while(1+next_value != 1){
		epsilon = next_value;
		next_value = next_value / 2;
	}

	return epsilon;
}

//main function
int main() {
	//run function for single and double precision
	float e_single = get_epsilon<float>();
	double e_double = get_epsilon<double>();
	
	//print results
	cout << "Single Precision: " << e_single << endl;
	cout << "Double Precision: " << e_double << endl;

	return 0;
}
